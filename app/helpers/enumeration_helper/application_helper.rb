module EnumerationHelper
  module ApplicationHelper

    # @param [Enumerable] collection 列挙する要素群
    # @param [String] separator セパレータ文字列
    # @return [ActiveSupport::SafeBuffer] 列挙したもの
    def enumerate(collection, separator: "")
      separator = content_tag(:span, separator, class: %w(enumeration separator)) if separator

      capture do
        collection.each_with_index do |item, i|
          concat(separator) if separator && i > 0
          concat(item)
        end
      end
    end

    # @param [Integer] limit 列挙する最大数
    # @param [String] href 列挙情報要素のリンク先（任意）
    # @param [Object] info 列挙情報要素の内容
    # @yield [item] コレクションの各要素を加工するブロック
    def enumerate_until(collection, limit, info: DEFAULT_INFO, href: nil, **options, &block)
      sliced = collection.try(:limit, limit) || collection.slice(0, limit)
      sliced = sliced.map(&block) if block_given?

      capture do

        # 列挙
        concat(enumerate(sliced, options))

        # 列挙情報要素
        if sliced.size < collection.count
          content = EnumerationHelper.enumeration_info_content(info, collection, sliced)
          content = link_to(content, href) if href
          concat(content_tag(:span, content, class: %w(enumeration info)))
        end
      end
    end
  end
end
