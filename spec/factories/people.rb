FactoryBot.define do
  factory :person do
    sequence(:name){|n| "Name-#{n}"}
  end
end
