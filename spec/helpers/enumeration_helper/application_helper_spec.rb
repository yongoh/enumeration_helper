require 'rails_helper'

describe EnumerationHelper::ApplicationHelper do
  let(:collection){ %w(foo ほげ bar ぴよ baz) }
  let(:sep){ '<span class="enumeration separator"></span>' }

  describe "#enumerate" do
    context "文字列の配列を渡した場合" do
      it "は、セパレータ要素を挟んで連結したものを返すこと" do
        expect(h.enumerate(collection)).to eq(collection.join(sep))
      end
    end

    context "HTML要素の配列を渡した場合" do
      subject do
        h.enumerate([
          h.content_tag(:span, "foo"),
          h.content_tag(:span, "ほげ"),
          h.content_tag(:span, "bar"),
          h.content_tag(:span, "ぴよ"),
          h.content_tag(:span, "baz"),
        ])
      end

      it "は、セパレータ要素を挟んで連結したものを返すこと" do
        is_expected.to eq([
          "<span>foo</span>",
          "<span>ほげ</span>",
          "<span>bar</span>",
          "<span>ぴよ</span>",
          "<span>baz</span>",
        ].join(sep))
      end
    end

    context "オプション`:separator`を渡した場合" do
      context "文字列を渡した場合" do
        let(:sep){ '<span class="enumeration separator">セ</span>' }

        it "は、渡した文字列を内容に持つセパレータ要素を挟んで連結したものを返すこと" do
          expect(h.enumerate(collection, separator: "セ")).to eq(collection.join(sep))
        end
      end

      context "`nil`を渡した場合" do
        it "は、セパレータ要素を挟まず連結したものを返すこと" do
          expect(h.enumerate(collection, separator: nil)).to eq(collection.join)
        end
      end
    end
  end

  describe "#enumerate_until" do
    shared_examples_for "enumerate all" do
      it "は、コレクションの全ての要素を連結したものを返すこと" do
        is_expected.to eq(collection.join(sep))
      end

      it "は、列挙情報要素を返さないこと" do
        is_expected.not_to have_tag(".enumeration.info")
      end
    end

    let(:info_element){ '<span class="enumeration info">...(3/5)</span>' }

    context "第2引数にコレクションの総数より多い数を渡した場合" do
      subject do
        h.enumerate_until(collection, 10)
      end

      it_behaves_like "enumerate all"
    end

    context "第2引数にコレクションの総数と同じ数を渡した場合" do
      subject do
        h.enumerate_until(collection, 5)
      end

      it_behaves_like "enumerate all"
    end

    context "第2引数にコレクションの総数より少ない数を渡した場合" do
      context "第1引数に文字列の配列を渡した場合" do
        it "は、渡した数だけ連結したものと列挙情報要素を返すこと" do
          expect(h.enumerate_until(collection, 3)).to eq(%w(foo ほげ bar).join(sep) + info_element)
        end
      end

      context "第1引数に`ActiveRecord::Relation`を渡した場合" do
        before do
          create_list(:person, 5)
        end

        let(:collection){ Person.all }

        it "は、渡した数だけ連結したものと列挙情報要素を返すこと" do
          expect(h.enumerate_until(collection, 3, &:name)).to eq([
            collection[0].name,
            collection[1].name,
            collection[2].name,
          ].join(sep) + info_element)
        end
      end
    end

    context "ブロックを渡した場合" do
      subject do
        h.enumerate_until(collection, 10){|x| "[#{x}]" }
      end

      it "は、コレクションの各要素をブロックの手続きで加工すること" do
        is_expected.to eq(%w([foo] [ほげ] [bar] [ぴよ] [baz]).join(sep))
      end
    end

    context "オプション`:info`を渡した場合" do
      subject do
        h.enumerate_until(collection, 3, info: info)
      end

      context "文字列を渡した場合" do
        let(:info){ "%<size>d(´･ω･｀)%<total>d" }

        it "は、式展開した文字列を列挙情報要素の内容にすること" do
          is_expected.to eq(collection.slice(0, 3).join(sep) + '<span class="enumeration info">3(´･ω･｀)5</span>')
        end
      end

      context "HTML要素を渡した場合" do
        let(:info){ h.content_tag(:div, "(´･ω･｀)", id: "shobon") }

        it "は、渡したものを列挙情報要素の内容にすること" do
          is_expected.to eq(collection.slice(0, 3).join(sep) + '<span class="enumeration info"><div id="shobon">(´･ω･｀)</div></span>')
        end
      end

      context "手続きオブジェクトを渡した場合" do
        let(:info){
          ->(old_collection, new_collection){ "#{old_collection.last}(´･ω･｀)#{new_collection.first}" }
        }

        it "は、手続きの戻り値を情報要素の内容にすること" do
          is_expected.to eq(collection.slice(0, 3).join(sep) + '<span class="enumeration info">baz(´･ω･｀)foo</span>')
        end
      end
    end

    context "オプション`:href`を渡した場合" do
      context "`nil`を渡した場合 (default)" do
        it "は、列挙情報要素にリンクを貼らないこと" do
          expect(h.enumerate_until(collection, 3, href: nil)).to have_tag(".enumeration.info", text: "...(3/5)", count: 1){
            without_tag("a")
          }
        end
      end

      context "URL文字列を渡した場合" do
        it "は、列挙情報要素にそのURLへのリンクを貼ること" do
          expect(h.enumerate_until(collection, 3, href: "/animals")).to have_tag(".enumeration.info", count: 1){
            with_tag("a", with: {href: "/animals"}, text: "...(3/5)")
          }
        end
      end
    end
  end
end
