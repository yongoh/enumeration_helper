require 'enumeration_helper/engine'

module EnumerationHelper

  # 情報要素のフォーマット
  DEFAULT_INFO = "...(%<size>d/%<total>d)".freeze

  class << self

    # @param [Object] content 内容の素
    # @param [Enumerable] old_collection 加工前のコレクション
    # @param [Enumerable] new_collection 加工後のコレクション
    # @return [String,ActiveSupport::SafeBuffer] 情報要素の内容
    def enumeration_info_content(content, old_collection, new_collection)
      case content
      when Proc then content.call(old_collection, new_collection)
      when ActiveSupport::SafeBuffer then content
      when String then sprintf(content, size: new_collection.size, total: old_collection.count)
      else content
      end
    end
  end
end
